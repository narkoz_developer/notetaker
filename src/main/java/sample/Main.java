package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.controllers.NotesController;
import sample.models.Note;
import sample.models.NoteInfo;
import sample.service.MainService;
import sample.service.NotesService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        final Parameters params = getParameters();
        final List<String> parameters = params.getRaw();
        primaryStage.setTitle("Note Taker");
        String pathToFile = !parameters.isEmpty() ? parameters.get(0) : null;

        if (Objects.nonNull(pathToFile)){
/*
            pathToFile = pathToFile.replace("\\","\\\\");
*/
            Path path = Paths.get(pathToFile);
            if (Files.exists(path)){
            try {
                NoteInfo info = MainService.getNoteInfo(pathToFile);
                if (Objects.nonNull(info)) {
                    openNote(info,primaryStage);
                } else {
                    openNote(NotesService.readNote(Paths.get(pathToFile)),primaryStage);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            primaryStage.show();
            } else {
                openMainMenu(primaryStage);
            }

        } else {
           openMainMenu(primaryStage);
        }
    }


    public static void main(String[] args) {
        launch(args);

    }

    public void openNote(NoteInfo info, Stage primaryStage){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("note.fxml"));
            Parent root = loader.load();
            NotesController controller = loader.getController();
            controller.loadNote(info);
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.show();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void openNote(Note note,Stage primaryStage){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("note.fxml"));
            Parent root = loader.load();
            NotesController controller = loader.getController();
            controller.noteTitle.setText(note.getTitle());
            controller.noteContent.setText(note.getContent());
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.show();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openMainMenu(Stage primaryStage){
        Parent root = null;
        try {
            root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("main.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }
}
