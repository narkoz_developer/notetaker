package sample.models;


import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Note {

    @EqualsAndHashCode.Include
    @ToString.Include
    private String title;
    @EqualsAndHashCode.Include
    @ToString.Include
    private String content;
    @EqualsAndHashCode.Include
    @ToString.Include
    private Date creationDate;
    @EqualsAndHashCode.Include
    @ToString.Include
    private Date updateDate;


}
