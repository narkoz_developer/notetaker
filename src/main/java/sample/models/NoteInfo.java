package sample.models;

import lombok.*;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NoteInfo {

    @EqualsAndHashCode.Exclude
    private String title;
    @EqualsAndHashCode.Include
    private Date creationDate;

    @EqualsAndHashCode.Exclude
    private Date updateDate;
    @EqualsAndHashCode.Include
    private String pathTo;

    public NoteInfo(Note note, String pathTo){
        this.title = note.getTitle();
        this.creationDate = note.getCreationDate();
        this.pathTo = pathTo;
    }
}
