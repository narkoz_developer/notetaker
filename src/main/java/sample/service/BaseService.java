package sample.service;

import com.fasterxml.jackson.databind.ObjectMapper;

public class BaseService {
    public static ObjectMapper getObjectMapper(){
        return new ObjectMapper();
    }
}
