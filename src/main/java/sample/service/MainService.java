package sample.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import sample.AppConstants;
import sample.models.NoteInfo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class MainService extends BaseService {

    public static List<NoteInfo> loadNotesList() {
        Path path = getAppFilePath();
        if (Files.exists(path)) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                return mapper.readValue(path.toFile(),
                        new TypeReference<List<NoteInfo>>() {
                        });
            } catch (IOException e) {
                return new ArrayList<>();
            }
        }
        return new ArrayList<>();

    }

    public static NoteInfo getNoteInfo(String notePath) {
        Path path = getAppFilePath();
        if (Files.exists(path)) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                List<NoteInfo> noteInfos = mapper.readValue(path.toFile(),
                        new TypeReference<List<NoteInfo>>() {
                        });
                return noteInfos.stream()
                        .filter(noteInfo -> noteInfo.getPathTo().equals(notePath))
                        .findFirst()
                        .orElseGet(null);

            } catch (IOException e) {
                return null;
            }
        }
        return null;
    }

    public static void addNoteInfo(NoteInfo noteInfo) {
        List<NoteInfo> noteInfos = loadNotesList();
        noteInfos.add(noteInfo);
        try {
            getObjectMapper().writeValue(getAppFilePath().toFile(), noteInfos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void updateNoteInfo(NoteInfo noteInfo) {
        List<NoteInfo> noteInfos = loadNotesList();
        int index = noteInfos.indexOf(noteInfo);
        noteInfos.set(index, noteInfo);
        try {
            getObjectMapper().writeValue(getAppFilePath().toFile(), noteInfos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void removeNoteInfo(NoteInfo noteInfo) {
        List<NoteInfo> noteInfos = loadNotesList();
        noteInfos.remove(noteInfo);
        try {
            getObjectMapper().writeValue(getAppFilePath().toFile(), noteInfos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static Path getAppFilePath() {
        try {
            Path path = Paths.get(AppConstants.APP_PATH);
            if (!Files.exists(path)) {
                Path parentDirectory = path.getParent();
                if (!Files.exists(parentDirectory)) {
                    Files.createDirectories(parentDirectory);
                }
                Files.createFile(path);
            }
            return Paths.get(AppConstants.APP_PATH);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
