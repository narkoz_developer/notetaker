package sample.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import sample.models.Note;
import sample.models.NoteInfo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

public class NotesService {


    public static boolean writeNote(Note note, Path directory) {
        Path file = getPathNotExists(note.getTitle(), directory);
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(file.toFile(), note);
            MainService.addNoteInfo(
                    NoteInfo.builder()
                            .creationDate(new Date())
                            .pathTo(file.toFile().getAbsolutePath())
                            .title(note.getTitle())
                            .build()
            );
            return true;
        } catch (IOException e) {
            System.out.println("Error writing note: " + e.getMessage());
            return false;
        }
    }


    private static Path getPathNotExists(String title, Path directory) {
        Path file = Paths.get(String.format("%s/%s.nttkr", directory.toAbsolutePath(), title));
        int index = 1;
        while (Files.exists(file)) {
            file = Paths.get(String.format("%s/%s%d.nttkr", directory.toAbsolutePath(), title, index));
        }

        return file;
    }

    public static Note readNote(Path file) {
        if (Files.exists(file) && Files.isReadable(file)) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                return mapper.readValue(file.toFile(), Note.class);
            } catch (IOException e) {
                System.out.println("Error reading note: " + e.getMessage());
                return null;
            }
        } else {
            System.out.printf("Error reading file : file %s does not exits or is not readable", file.getFileName());
            return null;
        }
    }

    public static boolean deleteNote(Path note) {
        try {
            return Files.deleteIfExists(note);
        } catch (IOException e) {
            System.out.println("Error occurred :" + e.getMessage());
            return false;
        }
    }

    public static boolean updateNote(NoteInfo info, Note note) {
        Path file = Paths.get(info.getPathTo());
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(file.toFile(), note);
            info.setUpdateDate(new Date());
            info.setTitle(note.getTitle());
            MainService.updateNoteInfo(info);
            return true;
        } catch (IOException e) {
            System.out.println("Error updating  note: " + e.getMessage());
            return false;
        }
    }
}
