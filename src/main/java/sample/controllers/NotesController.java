package sample.controllers;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import sample.models.Note;
import sample.models.NoteInfo;
import sample.service.MainService;
import sample.service.NotesService;
import sample.utils.AlertUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Objects;
import java.util.ResourceBundle;

public class NotesController implements Initializable {

    private static final String SAVE_NOTE_TITLE = "Save note";
    private static final String SUCCESS_TITLE = "Successfully saved the note";
    private static final String SUCCESS_CONTENT = "Note %s was successfully saved";
    private static final String UPDATE_CONTENT = "Note %s was successfully updated";
    private static final String ERROR_TITLE = "Error";
    private static final String ERROR_CONTENT = "Error saving the note.";

    public TextArea noteContent;
    public TextField noteTitle;
    public ToggleButton toggleMode;

    private BooleanProperty update;
    private NoteInfo info;


    @FXML
    public void clearNote() {
        noteContent.setText("");
        noteTitle.setText("");
    }

    public void saveNote(ActionEvent actionEvent) {
        if (!update.getValue()) {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setTitle(SAVE_NOTE_TITLE);
            Note note = new Note(noteTitle.getText(), noteContent.getText(), new Date(), new Date());
            try {

                File file = directoryChooser.showDialog(noteTitle.getScene().getWindow());
                if (NotesService.writeNote(note, file.toPath())) {
                    AlertUtils.showSuccessAlert(SUCCESS_TITLE, String.format(SUCCESS_CONTENT, note.getTitle()));
                    clearNote();
                    goToMain(actionEvent);
                } else AlertUtils.showErrorAlert(ERROR_TITLE, ERROR_CONTENT);
            } catch (Exception e) {
                System.out.println("Exception occurred  "+e.getMessage());
            }
        } else {
            try {
                if (NotesService.updateNote(info,Note.builder()
                .content(noteContent.getText())
                .creationDate(info.getCreationDate())
                .title(noteTitle.getText())
                .updateDate(new Date())
                .build())){
                    AlertUtils.showSuccessAlert(SUCCESS_TITLE, String.format(UPDATE_CONTENT, noteTitle.getText()));
                }
            }catch (Exception e){}
        }
    }


    public void goToMain(ActionEvent actionEvent) throws IOException {
        Parent main = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("main.fxml")));
        Scene mainScene = new Scene(main);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(mainScene);
        window.show();
    }


    public void loadNote(NoteInfo noteInfo) {

        if (Files.exists(Paths.get(noteInfo.getPathTo()))) {
            this.update.setValue(true);
            this.info = noteInfo;
            this.toggleMode.setVisible(true);
            Note note = NotesService.readNote(Paths.get(noteInfo.getPathTo()));
            System.out.println(note);
            this.noteTitle.setText(note.getTitle());
            this.noteContent.setText(note.getContent());
        } else {
            System.out.println("Note does not exists");
            MainService.removeNoteInfo(noteInfo);
        }
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        /* toggleMode.setText((update)?"Update Mode":"Save Mode");*/

        this.update = new SimpleBooleanProperty();
        this.update.setValue(false);
        this.toggleMode.setVisible(false);
        this.toggleMode.setText(this.update.getValue() ? "Update Mode" : "Create Mode");
        this.update.addListener(((observable, oldVal, newVal) ->
        {
            this.toggleMode.setText((newVal) ? "Update Mode" : "Create Mode");
            this.toggleMode.setSelected(newVal);
        }));
        this.toggleMode.selectedProperty().addListener(((observable, oldValue, newValue) ->
                update.setValue(newValue)));
    }

}
