package sample.controllers;


import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import sample.models.NoteInfo;
import sample.service.MainService;
import sample.service.NotesService;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    public ListView<NoteInfo> notesList;


    public void openNewNote(ActionEvent actionEvent) throws IOException {
            Parent main = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("note.fxml")));
            Scene mainScene = new Scene(main);
            Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            window.setScene(mainScene);
            window.show();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        notesList.setCellFactory(param -> new ListCell<NoteInfo>() {
            @Override
            protected void updateItem(NoteInfo item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || Objects.isNull(item)) {
                } else {
                    setText(String.format("%s   ( created  at " +
                            "%s )", item.getTitle(), item.getCreationDate().toString()));
                }
            }
        });
        notesList.getItems().addAll(MainService.loadNotesList());

        notesList.setOnKeyPressed(event -> {
            if (event.getCode()==KeyCode.ENTER){
                openNote(notesList.getSelectionModel().getSelectedItem(), event);

            }
            if (event.getCode()== KeyCode.DELETE){
                NoteInfo info = notesList.getSelectionModel().getSelectedItem();
                MainService.removeNoteInfo(info);
                NotesService.deleteNote(Paths.get(info.getPathTo()));
                notesList.getItems().remove(notesList.getSelectionModel().getSelectedItem());
            }
        });
    }

    private void openNote(NoteInfo noteInfo, Event event) {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("note.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            NotesController controller = loader.getController();
            controller.loadNote(noteInfo);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
