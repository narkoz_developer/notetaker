package sample.utils;

import javafx.scene.control.Alert;

public class AlertUtils {

    public static void showErrorAlert(String title, String content){
        showAlert(title,content, Alert.AlertType.ERROR);
    }
    public static void showSuccessAlert(String title, String content){
        showAlert(title,content, Alert.AlertType.CONFIRMATION);
    }
    public static void showInformationAlert(String title, String content){
        showAlert(title,content, Alert.AlertType.INFORMATION);
    }

    private static void showAlert(String title, String content, Alert.AlertType type){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setContentText(content);
        alert.show();
    }
}
